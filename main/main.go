package main

import (
	"net/http"
	"net/http/httptest"

	"gitlab.com/birowo/mux"
)

func callback(pathSet string) http.HandlerFunc {
	return func(rw http.ResponseWriter, rq *http.Request) {
		rw.Write([]byte("path set: " + pathSet + "\npath: " + rq.URL.Path + "\n"))
		if _rw, ok := rw.(mux.RW); ok {
			for _, param := range _rw.Params {
				switch param {
				case "divByZero": //recover test
					zero := 0
					_ = 0 / zero
				default:
					rw.Write([]byte("param: " + param + "\n"))
				}
			}
			if _rw.Wildcard != "" {
				rw.Write([]byte("wildcard: " + _rw.Wildcard + "\n"))
			}
		}
	}
}

func main() {
	pathsSet := []string{
		"/ab/:/bc/:/cd",
		"/bc/:/cd/*",
		"/ab/bc/:/*",
		"/bc/:/cde/e/:",
		"/ab/:/bc/cd/*",
		"/c/:/d/e/:",
		"/c/:/d/e",
		"/c/:/d/*",
		"/",
	}
	m := mux.Config(nil, nil, nil)
	for _, pathSet := range pathsSet {
		m.Set("GET", pathSet, callback(pathSet))
	}

	srvr := httptest.NewServer(m)
	defer srvr.Close()

	paths := []string{
		"/ab/AB/bc/CD/cd",
		"/bc/BC/cd/123",
		"/ab/bc/BC/123",
		"/bc/BC/cde/e/E",
		"/ab/AB/bc/cd/123",
		"/c/123/d/e/divByZero",
		"/c/234/d/e",
		"/c/345/d/ABC",
		"/",
	}
	for _, path := range paths {
		rs, err := http.Get(srvr.URL + path)
		if err != nil {
			println(err.Error())
			return
		}
		body := make([]byte, rs.ContentLength)
		rs.Body.Read(body)
		rs.Body.Close()
		println(string(body))
	}
}
