package mux

import (
	"errors"
	"fmt"
	"net/http"
)

type Trie []struct {
	Chr  byte
	Prfx string
	CB   http.HandlerFunc
	Nxt  Trie
}

func prms(path string, callback, notFoundCb http.HandlerFunc, t Trie) (prmsNum int) {
	pathLen := len(path)
	end := 1
	for {
		chr := path[end]
		if chr > 126 {
			return -1
		}
		t[chr].Chr = chr
		if chr == '*' {
			t[chr].CB = callback
			return
		}
		end++
		if chr == prmSprtr {
			if end < pathLen && path[end] != '/' {
				return -1
			}
			prmsNum++
		}
		bgn := end
		for end < pathLen && path[end] != prmSprtr && path[end] != '*' {
			end++
		}
		t[chr].Prfx = path[bgn:end]
		if end == pathLen {
			t[chr].CB = callback
			return
		}
		t[chr].CB = notFoundCb
		t[chr].Nxt = make(Trie, 127)
		t = t[chr].Nxt
	}
}

type Route struct {
	get, post, put, patch, del                               Trie
	prmsNum                                                  int
	getRootCb, postRootCb, putRootCb, patchRootCb, delRootCb http.HandlerFunc
	NotAllowCb, NotFoundCb, BadRqstCb                        http.HandlerFunc
}

func (r *Route) Set(method, path string, callback http.HandlerFunc) error {
	if path == "" || path[0] != '/' {
		return errors.New("invalid path: " + path)
	}
	if callback == nil {
		return errors.New("invalid callback")
	}
	if path == "/" {
		switch method {
		case "GET":
			r.getRootCb = callback
		case "POST":
			r.postRootCb = callback
		case "PUT":
			r.putRootCb = callback
		case "PATCH":
			r.patchRootCb = callback
		case "DELETE":
			r.delRootCb = callback
		default:
			return errors.New("invalid method")
		}
		return nil
	}
	t2 := make(Trie, 127)
	prmsNum := prms(path, callback, r.NotFoundCb, t2)
	if prmsNum == -1 {
		return errors.New("invalid path: " + path)
	}
	if r.prmsNum < prmsNum {
		r.prmsNum = prmsNum
	}
	var t1 Trie
	switch method {
	case "GET":
		t1 = r.get
		if t1 == nil {
			r.get = t2
			return nil
		}
	case "POST":
		t1 = r.post
		if t1 == nil {
			r.post = t2
			return nil
		}
	case "PUT":
		t1 = r.put
		if t1 == nil {
			r.put = t2
			return nil
		}
	case "PATCH":
		t1 = r.patch
		if t1 == nil {
			r.patch = t2
			return nil
		}
	case "DELETE":
		t1 = r.del
		if t1 == nil {
			r.del = t2
			return nil
		}
	default:
		return errors.New("invalid method")
	}
	idx, pathLen := 1, len(path)
	for idx < pathLen {
		chr := path[idx]
		if t1[chr].Chr != chr {
			t1[chr] = t2[chr]
			return nil
		}
		if t1[chr].Prfx != t2[chr].Prfx {
			prfx1Len, prfx2Len := len(t1[chr].Prfx), len(t2[chr].Prfx)
			i := 1
			for i < prfx1Len && i < prfx2Len && t1[chr].Prfx[i] == t2[chr].Prfx[i] {
				i++
			}
			if i == prfx1Len {
				_chr := t2[chr].Prfx[i]
				if t1[chr].Nxt == nil {
					t1[chr].Nxt = make(Trie, 127)
				}
				t1 = t1[chr].Nxt
				t1[_chr] = t2[chr]
				t1[_chr].Chr = _chr
				t1[_chr].Prfx = t2[chr].Prfx[i+1:]
				return nil
			}
			if i == prfx2Len {
				_chr := t1[chr].Prfx[i]
				if t2[chr].Nxt == nil {
					t2[chr].Nxt = make(Trie, 127)
				}
				_t := t2[chr].Nxt
				_t[_chr] = t1[chr]
				_t[_chr].Chr = _chr
				_t[_chr].Prfx = t1[chr].Prfx[i+1:]
				t1[chr] = t2[chr]
				return nil
			}
			_t := make(Trie, 127)
			_chr := t1[chr].Prfx[i]
			_t[_chr] = t1[chr]
			_t[_chr].Chr = _chr
			_t[_chr].Prfx = t1[chr].Prfx[i+1:]
			_chr = t2[chr].Prfx[i]
			_t[_chr] = t2[chr]
			_t[_chr].Chr = _chr
			_t[_chr].Prfx = t2[chr].Prfx[i+1:]
			t1[chr].Prfx = t1[chr].Prfx[:i]
			t1[chr].CB = r.NotFoundCb
			t1[chr].Nxt = _t
			return nil
		}
		idx += len(t1[chr].Prfx) + 1
		t1 = t1[chr].Nxt
		t2 = t2[chr].Nxt
	}
	return errors.New("invalid path: " + path)
}

type RW struct {
	http.ResponseWriter
	Params   []string
	Wildcard string
}

const prmSprtr = ':'

func (r Route) ServeHTTP(rw http.ResponseWriter, rq *http.Request) {
	url := rq.URL
	defer func() {
		if rcvr := recover(); rcvr != nil {
			fmt.Println("path:", url.Path, "\nrecover:", rcvr)
		}
	}()

	if url.Path == "" || url.Path[0] != '/' {
		r.BadRqstCb(rw, rq)
		return
	}
	var t Trie
	switch rq.Method {
	case "GET":
		if url.Path == "/" {
			r.getRootCb(rw, rq)
			return
		}
		t = r.get
	case "POST":
		if url.Path == "/" {
			r.postRootCb(rw, rq)
			return
		}
		t = r.post
	case "PUT":
		if url.Path == "/" {
			r.putRootCb(rw, rq)
			return
		}
		t = r.put
	case "PATCH":
		if url.Path == "/" {
			r.patchRootCb(rw, rq)
			return
		}
		t = r.patch
	case "DELETE":
		if url.Path == "/" {
			r.delRootCb(rw, rq)
			return
		}
		t = r.del
	default:
		r.NotAllowCb(rw, rq)
		return
	}
	var params []string
	pathLen := len(url.Path)
	bgn := 1
	for {
		chr := url.Path[bgn]
		if chr > 126 {
			r.BadRqstCb(rw, rq)
			return
		}
		if t[chr].Chr == chr {
			bgn++
			end := bgn + len(t[chr].Prfx)
			if end > pathLen || t[chr].Prfx != url.Path[bgn:end] {
				r.NotFoundCb(rw, rq)
				return
			}
			if t[chr].Nxt == nil || bgn >= pathLen {
				t[chr].CB(RW{rw, params, ""}, rq)
				return
			}
			t = t[chr].Nxt
			bgn = end
		} else if t[prmSprtr].Chr == prmSprtr {
			_bgn := bgn
			for bgn < pathLen && url.Path[bgn] != '/' {
				bgn++
			}
			end := bgn + len(t[prmSprtr].Prfx)
			if end > pathLen || t[prmSprtr].Prfx != url.Path[bgn:end] {
				r.NotFoundCb(rw, rq)
				return
			}
			params = append(params, url.Path[_bgn:bgn])
			if t[prmSprtr].Nxt == nil || bgn >= pathLen {
				t[prmSprtr].CB(RW{rw, params, ""}, rq)
				return
			}
			t = t[prmSprtr].Nxt
			bgn = end
		} else if t['*'].Chr == '*' {
			t['*'].CB(RW{rw, params, url.Path[bgn:]}, rq)
			return
		} else {
			r.NotFoundCb(rw, rq)
			return
		}
	}
}
func _notAllowCb(rw http.ResponseWriter, rq *http.Request) {
	rw.WriteHeader(http.StatusMethodNotAllowed)
	rw.Write([]byte("method not allowed"))
}
func _notFoundCb(rw http.ResponseWriter, rq *http.Request) {
	rw.WriteHeader(http.StatusNotFound)
	rw.Write([]byte("resource not found"))
}
func _badRqstCb(rw http.ResponseWriter, rq *http.Request) {
	rw.WriteHeader(http.StatusBadRequest)
	rw.Write([]byte("bad request"))
}
func Config(notAllowCb, notFoundCb, badRqstCb http.HandlerFunc) (r *Route) {
	r = new(Route)
	if notAllowCb == nil {
		r.NotAllowCb = _notAllowCb
	}
	if notFoundCb == nil {
		r.NotFoundCb = _notFoundCb
	}
	if badRqstCb == nil {
		r.BadRqstCb = _badRqstCb
	}
	return
}
